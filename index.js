/**
 * EX1
 * đầu vào: lương 1 ngày 100.000
 * xử lý: lấy lưong 1 ngày * số ngày làm
 * đầu ra: tính số lương của nhân viên
 */

function tongLuong() {
  const luong1Ngay = 100000;
  var soNgayLam = document.getElementById("songaylam").value;

  var tongLuong = luong1Ngay * soNgayLam;
  console.log("tongLuong", tongLuong);
}

/**
 * EX2
 * đầu vào: nhập 5 số thực
 * xử lý: lấy tổng 5 số thực chia cho 5
 * đầu ra: giá trị trung bình của tổng 5 số thực
 */

function trungBinh() {
  var soThuc1 = document.getElementById("sothuc1").value * 1;
  var soThuc2 = document.getElementById("sothuc2").value * 1;
  var soThuc3 = document.getElementById("sothuc3").value * 1;
  var soThuc4 = document.getElementById("sothuc4").value * 1;
  var soThuc5 = document.getElementById("sothuc5").value * 1;

  var tinhTrungBinh = Math.floor(
    (soThuc1 + soThuc2 + soThuc3 + soThuc4 + soThuc5) / 5
  );
  console.log("tinhTrungBinh", tinhTrungBinh);
}

/**EX3
 * đầu vào: Gia trị của 1 USD đổi sang tiền việt là 23.500 VND
 * xử lý: Nhận dữ liệu người dùng nhập và nhân với 23.500 VND
 * đầu ra: Xuất ra số tiền sau khi quy đổi ra VND
 */

function thanhTien() {
  const tienVND = 23.5;
  var number = document.getElementById("number").value;

  var thanhTien = tienVND * number;
  console.log("Tiền nhận được là", thanhTien);
}

/**EX4
 * Đầu vào : Chiều dài và chiều rộng của hình chữ nhật đó
 * Xử lý: tính chu vi diện tích của hình chữ nhật đó từ dữ liệu nhập vào
 * Đầu ra: xuất ra kết quả của 2 phép tính đó
 */
function dienTich() {
  var chieuDai = document.getElementById("chieudai").value * 1;
  var chieuRong = document.getElementById("chieurong").value * 1;

  var dienTich = chieuDai * chieuRong;
  console.log("Diện tích", dienTich);
}
function chuVi() {
  var chieuDai = document.getElementById("chieudai").value * 1;
  var chieuRong = document.getElementById("chieurong").value * 1;
  var chuVi = (chieuDai + chieuRong) * 2;
  console.log("Chu vi", chuVi);
}

/**
 * EX5
 * Đầu vào: Nhận vào 1 số có 2 chữ số
 * Xử lý: lấy ra số đơn vị và số hàng chục từ 1 số có chữ số
 * Đầu ra: tính tổng 2 ký số đã nhận
 */

function tongKySo() {
  var num2 = document.getElementById("num2").value * 1;

  var donVi = num2 % 10;
  var hangChuc = Math.floor(num2 / 10);

  var result = donVi + hangChuc;
  console.log("Tổng số ký là", result);
}
